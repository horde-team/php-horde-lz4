php-horde-lz4 (1.0.10-10) unstable; urgency=medium

  * Team upload.
  * d/t/control: add php-dom. (Closes: #1028110)

 -- Anton Gladky <gladk@debian.org>  Sat, 07 Jan 2023 22:01:37 +0100

php-horde-lz4 (1.0.10-9) unstable; urgency=medium

  * Team upload.
  [ Mike Gabriel ]
  * d/watch: Switch to format version 4.

  [ Anton Gladky ]
  * d/salsa-ci.yml: use aptly, simplify.
  * d/patches: fix compilation against php8.1. (Closes: #1003473)
  * d/patches: fix autopkgtest.
  * d/control: update standards version to 4.6.2, no changes needed.
  * d/control: fix website protocol.
  * d/control: Switch BD from pear-horde-channel to pear-channels.
  * d/control: remove unnecessary BD version restriction.

 -- Anton Gladky <gladk@debian.org>  Thu, 05 Jan 2023 21:36:30 +0100

php-horde-lz4 (1.0.10-8) unstable; urgency=medium

  * d/patches: Add 1010_phpunit-8.x+9.x.patch. Fix tests with PHPUnit 8.x/9.x.
  * d/t/control: Require php-horde-test (>= 2.6.4+debian0-6~).

 -- Mike Gabriel <sunweaver@debian.org>  Mon, 19 Oct 2020 14:22:37 +0000

php-horde-lz4 (1.0.10-7) unstable; urgency=medium

  * d/control: Add to Uploaders: Juri Grabowski.
  * d/control: Bump DH compat level to version 13.
  * d/salsa-ci.yml: Add file with salsa-ci.yml and pipeline-jobs.yml calls.

 -- Mike Gabriel <sunweaver@debian.org>  Wed, 01 Jul 2020 17:39:56 +0200

php-horde-lz4 (1.0.10-6) unstable; urgency=medium

  * Re-upload to Debian. (Closes: #959355).

  * d/control: Add to Uploaders: Mike Gabriel.
  * d/control: Drop from Uploaders: Debian QA Group.
  * d/control: Bump Standards-Version: to 4.5.0. No changes needed.
  * d/control: Add Rules-Requires-Root: field and set it to 'no'.
  * d/upstream/metadata: Add file. Comply with DEP-12.
  * d/tests/control: Stop using deprecated needs-recommends restriction.
  * d/upstream/metadata: VCS upstream repo unknown.
  * d/copyright: Update copyright attributions.

 -- Mike Gabriel <sunweaver@debian.org>  Thu, 21 May 2020 09:17:13 +0200

php-horde-lz4 (1.0.10-5) unstable; urgency=medium

  * Bump debhelper from old 11 to 12.
  * d/control: Orphaning package (See #942282)

 -- Mathieu Parent <sathieu@debian.org>  Fri, 18 Oct 2019 21:14:45 +0200

php-horde-lz4 (1.0.10-4) unstable; urgency=medium

  * Update Standards-Version to 4.1.4, no change
  * Update Maintainer field

 -- Mathieu Parent <sathieu@debian.org>  Tue, 15 May 2018 21:31:54 +0200

php-horde-lz4 (1.0.10-3) unstable; urgency=medium

  * Update Standards-Version to 4.1.3, no change
  * Upgrade debhelper to compat 11
  * Update Vcs-* fields
  * Use secure copyright format URI
  * Replace "Priority: extra" by "Priority: optional"

 -- Mathieu Parent <sathieu@debian.org>  Fri, 06 Apr 2018 18:52:46 +0200

php-horde-lz4 (1.0.10-2) unstable; urgency=medium

  * Update Standards-Version to 3.9.8, no change
  * Updated d/watch to use https

 -- Mathieu Parent <sathieu@debian.org>  Wed, 08 Jun 2016 20:07:16 +0200

php-horde-lz4 (1.0.10-1) unstable; urgency=medium

  * New upstream version 1.0.10

 -- Mathieu Parent <sathieu@debian.org>  Sat, 26 Mar 2016 14:09:31 +0100

php-horde-lz4 (1.0.9-2) unstable; urgency=medium

  * Replace php5-dev and dh-php5 by php-dev and dh-php
  * Update Standards-Version to 3.9.7, no change
  * Use secure Vcs-* fields
  * Rebuild with newer pkg-php-tools for the PHP 7 transition
  * Replace php5-* by php-* in d/tests/control

 -- Mathieu Parent <sathieu@debian.org>  Sun, 20 Mar 2016 10:32:11 +0100

php-horde-lz4 (1.0.9-1) unstable; urgency=medium

  * New upstream version 1.0.9

 -- Mathieu Parent <sathieu@debian.org>  Thu, 04 Feb 2016 13:56:59 +0100

php-horde-lz4 (1.0.8-2) unstable; urgency=medium

  * Remove XS-Testsuite header in d/control
  * Update gbp.conf

 -- Mathieu Parent <sathieu@debian.org>  Mon, 10 Aug 2015 07:24:54 +0200

php-horde-lz4 (1.0.8-1) unstable; urgency=medium

  * Update Standards-Version to 3.9.6, no change
  * New upstream version 1.0.8

 -- Mathieu Parent <sathieu@debian.org>  Tue, 05 May 2015 21:42:28 +0200

php-horde-lz4 (1.0.7-2) unstable; urgency=medium

  * Fixed DEP-8 tests, by removing "set -x"

 -- Mathieu Parent <sathieu@debian.org>  Sat, 11 Oct 2014 15:56:45 +0200

php-horde-lz4 (1.0.7-1) unstable; urgency=medium

  * New upstream version 1.0.7
  * Build with system lz4 (Closes: #725224)
    - Depends on liblz4-dev
    - configure --with-liblz4
    - depends on pkg-php-tools >= 1.23, previous version didn't pass configure
      options

 -- Mathieu Parent <sathieu@debian.org>  Sat, 04 Oct 2014 07:01:08 +0200

php-horde-lz4 (1.0.6-3) unstable; urgency=medium

  * Fixed DEP-8 tests

 -- Mathieu Parent <sathieu@debian.org>  Sat, 13 Sep 2014 16:32:45 +0200

php-horde-lz4 (1.0.6-2) unstable; urgency=medium

  * Update Standards-Version, no change
  * Update Vcs-Browser to use cgit instead of gitweb
  * Add dep-8 (automatic as-installed package testing)

 -- Mathieu Parent <sathieu@debian.org>  Tue, 26 Aug 2014 23:27:44 +0200

php-horde-lz4 (1.0.6-1) unstable; urgency=medium

  * New upstream version 1.0.6

 -- Mathieu Parent <sathieu@debian.org>  Fri, 04 Jul 2014 08:48:11 +0200

php-horde-lz4 (1.0.3-1) unstable; urgency=medium

  * New upstream version 1.0.3

 -- Mathieu Parent <sathieu@debian.org>  Sat, 15 Feb 2014 13:51:23 +0100

php-horde-lz4 (1.0.2-2) unstable; urgency=low

  * Use dh_php5 addon, to enable the extension automatically
    - Build-depend on dh-php5
    - --with php5 in d/rules
    - debian/php5
    - debian/horde_lz4.ini

 -- Mathieu Parent <sathieu@debian.org>  Sat, 02 Nov 2013 17:14:07 +0100

php-horde-lz4 (1.0.2-1) unstable; urgency=low

  * New upstream version 1.0.2

 -- Mathieu Parent <sathieu@debian.org>  Sun, 11 Aug 2013 13:33:14 +0200

php-horde-lz4 (1.0.1-1) unstable; urgency=low

  * New upstream version 1.0.1
  * License is now Expat (and thus dfsg-compatible)

 -- Mathieu Parent <sathieu@debian.org>  Mon, 15 Jul 2013 12:17:54 +0200

php-horde-lz4 (1.0.0-1) unstable; urgency=low

  * New upstream version 1.0.0

 -- Mathieu Parent <sathieu@debian.org>  Mon, 27 May 2013 17:28:07 +0200

php-horde-lz4 (1.0.0~beta2-1) unstable; urgency=low

  * Horde's horde_lz4 package
  * Initial packaging (Closes: #704955)

 -- Mathieu Parent <sathieu@debian.org>  Mon, 27 May 2013 17:26:35 +0200
